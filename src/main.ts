import { NestFactory } from '@nestjs/core';
import { INestApplication } from '@nestjs/common';
import { ApplicationModule } from './app/app.module';
import { environment } from './environments/environment';
import { whitelist } from '@nestling/cors';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

(async () => {
  const app: INestApplication = await NestFactory.create(ApplicationModule);

  app.enableCors(whitelist(environment.whitelist, {}));

  const config = new DocumentBuilder()
    .setTitle('Token Service API')
    .setDescription('')
    .setVersion('1.0')
    .addTag('token')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(environment.port, () =>
    console.log(
      `@authumn/token-service is listening on port ${environment.port}`,
    ),
  );
})();
