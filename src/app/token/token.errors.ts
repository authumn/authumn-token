import { IErrorMessage } from '@nestling/errors';

import * as oauth from 'oauth2-server';

export const oauth2ServerErrorHandler = {
  type: oauth.OAuthError,
  handler: (exception: oauth.OAuthError): IErrorMessage => {
    return {
      code: exception.name,
      message: exception.message,
      statusCode: exception.code,
    };
  },
};
