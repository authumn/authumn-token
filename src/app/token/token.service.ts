import * as jwt from 'jsonwebtoken';
import * as uuid from 'uuid';
import { Injectable, Inject } from '@nestjs/common';
import { RedisClientType } from 'redis';
import { environment } from '../../environments/environment';
import { User } from '../models/types';

/**
 * TODO: double check this
 *
 * @param userKey
 * @returns {string}
 */
export function secret(userKey: string): string {
  return [environment.token.secret, userKey].join(':');
}

export function createSessionKey(...args: string[]) {
  return args.join(':');
}

export function createToken(
  user: any,
  userKey: string,
  payload: string | Buffer | object,
): string {
  return jwt.sign(payload, secret(userKey), {
    algorithm: 'HS256',
    expiresIn: environment.token.expiration_time,
    // jwtid: environment.token.jwtid || ''
    // notBefore:
    // audience:
    // issuer:
    // jwtid:
    // subject:
    // noTimestamp
    // header
    // keyId
  });
}

@Injectable()
export class TokenService {
  constructor(
    @Inject('RedisToken')
    private redis: RedisClientType,
  ) {}

  /**
   * @param user
   * @param deviceId
   * @returns {Promise<string>}
   */
  async sign(user: User, deviceId: string | null = null): Promise<string> {
    const userKey = uuid.v4();
    const issuedAt = Math.floor(Date.now() / 1000);

    const payload = {
      _id: user._id,
      email: user.email,
      deviceId,
      iat: issuedAt,
    };

    const token = createToken(user, userKey, payload);

    const sessionKey = createSessionKey(
      environment.client.id,
      user._id,
      deviceId,
      issuedAt.toString(),
    );

    const writeSessionKey = await this.redis.set(sessionKey, userKey);

    if (writeSessionKey === 'OK') {
      const expireAsyncResult = await this.redis.expire(
        sessionKey,
        environment.token.expiration_time,
      );

      if (expireAsyncResult) {
        return token;
      }

      throw Error('Failed to set expire.');
    }

    throw Error('Failed to write session key.');
  }

  /**
   * Get Session from redis store
   *
   * @param {string} sessionKey
   * @returns {Promise<any>}
   */
  async get(sessionKey: string): Promise<string> {
    return this.redis.get(sessionKey);
  }

  /**
   * Remove session from redis store
   *
   * @param {string} sessionKey
   * @returns {Promise<any>}
   */
  async delete(sessionKey: string): Promise<any> {
    return this.redis.del(sessionKey);
  }

  /**
   * Get token list as in redis
   *
   * @returns {Promise<void>}
   */
  async keys(): Promise<string[]> {
    return this.redis.keys('*');
  }
}
