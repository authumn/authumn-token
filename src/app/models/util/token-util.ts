import * as crypto from 'crypto';

/**
 * Generate random token.
 *
 * @returns {Promise<string>}
 */
export function generateRandomToken(): string {
  const buffer = crypto.randomBytes(256);
  return crypto.createHash('sha1').update(buffer).digest('hex');
}
