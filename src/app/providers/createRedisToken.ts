import { createClient, RedisClientType } from 'redis';
import { environment } from '../../environments/environment';

export function createRedisToken(config: any) {
  // redis.ClientOpts) {
  return {
    provide: 'RedisToken',

    useFactory: async (): Promise<RedisClientType> => {
      const client: RedisClientType = createClient(config);

      await client.connect();

      client.on('connect', () => {
        console.log('Redis connected');
      });

      client.on('reconnecting', (delay, attempt) => {
        console.log(`Lost connection: delay(${delay} attempt(${attempt}`);
      });

      client.on('error', (error) => {
        console.error(error);
      });

      await client.select(parseInt(environment.redis.database.toString()));

      return client;
    },
  };
}
